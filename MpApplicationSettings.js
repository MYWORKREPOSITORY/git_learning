//
//  ApplicationSettings.js
//  NSTools
//
//  Created by Lee Horrocks on 2013-11-12.
//  Copyright 2013 2066795 Ontario Ltd. All rights reserved.
//
// Usage:
// var app = new ApplicationSettings({ "application":"your_id_here"});
// app.setSetting('new','local value');
// app.store();


/*global nlobjSearchFilter nlobjSearchColumn nlapiSearchRecord nlapiSubmitFields nlapiSubmitRecord nlapiCreateRecord nlapiLogExecution console */

// TODO Provide an API to set and update global settings 

/** 
 * Load up your application settings 
 *
 * The options provides the 'application' key which identifies a namespace from which persistent settings are loaded
 * Additional keys in the options hash will replace stored settings.  These are not written during a store operation.
 * 
 * @param {Object} options
 */
function ApplicationSettings(oOptions) {
    if (typeof oOptions === "undefined") {
        throw new Error("ApplicationSettings requires an options object.");
    } 
    if (typeof oOptions.application === 'undefined' ||
        oOptions.application.length < 1)
    {
        throw new Error("ApplicationSettings options initializer requires an 'application' key value.");
    }
    this.init(oOptions);
}

ApplicationSettings.NS_RECORD_ID = "customrecord_mp_application_settings";   // custom record id that shouldn't ever need to be changed.
ApplicationSettings.APP_CATEGORY_KEY = "application";  // input options key to identify the app namespace
ApplicationSettings.CATEGORY_GLOBAL_ID = "#global";  // # prefix is necessary for the correct sort order

/** Client script validation function 
 *  Also helps tie the script to custom record for bundling 
 */
ApplicationSettings.categoryValidation = function(sType, sName) {
    if (sName == 'custrecord_application_setting_category') {
        var sCategory = nlapiGetFieldValue('custrecord_application_setting_category');
        if ("#" === sCategory.charAt(0) && sCategory !== ApplicationSettings.CATEGORY_GLOBAL_ID) {
            alert("Only the global category "+ApplicationSettings.CATEGORY_GLOBAL_ID+" can start with #.");
            return false;
        }
    }
    return true;
};

/**
 * Initialize the ApplicationSettings object
 *
 * @param {Object} oOptions Caller settings overrides
 */
ApplicationSettings.prototype.init = function(oOptions) {
    var self = this;
    self.applicationSettings = {};
    self.updatedKeys = [];
    self.load(oOptions[ApplicationSettings.APP_CATEGORY_KEY]);  // override 'load' if you want to do your own data store.
    // User defined settings replace stored values
    self.applicationSettings = self.extend(self.applicationSettings, oOptions);
};

/**
 * Load settings values from a NetSuite custom table.
 *
 * The custom record implementation supports a #global category that is processed first.  Values for Keys in the global namespace are then
 * replaced with application namespace specific values where their keys are the same.
 *
 * @param {String} sApplicationCategory specifies the namespace for the application

 * @returns {void} The applicationSettings hash is updated
 */
ApplicationSettings.prototype.load = function(sApplicationCategory) {
    var self = this;
    this.log('AUDIT', "ApplicationSettings.load", "sApplicationCategory: " + sApplicationCategory);
    this.log('AUDIT', "ApplicationSettings.load", "ApplicationSettings.CATEGORY_GLOBAL_ID: " + ApplicationSettings.CATEGORY_GLOBAL_ID);
    this.log('AUDIT', "ApplicationSettings.load", "NS_RECORD_ID: " +ApplicationSettings.NS_RECORD_ID);
    try {
        if (typeof nlapiSearchRecord !== "undefined") {
            
            // CODE FOR REFERENCE.  Saved Search because expressions don't work server side from code
            // ********************************
            // var filters = [
            //     new nlobjSearchFilter('custrecord_application_setting_category', null, 'contains', ApplicationSettings.CATEGORY_GLOBAL_ID ),
            //     new nlobjSearchFilter('custrecord_application_setting_category', null, 'contains', sApplicationCategory )
            // ];
            // filters[0].isor = true;   // Not directly supported by the api
            // var columns = [
            //     new nlobjSearchColumn('custrecord_application_setting_category'),
            //     new nlobjSearchColumn('custrecord_application_setting_key'),
            //     new nlobjSearchColumn('custrecord_application_setting_value')
            // ];
            // // need to set order because if you don't its random - contrary to the API spec.  <sigh>
            // columns[0].setSort(false);

            //Define search filter expression
            //var filterExpression = [ 
            //    [ 'custrecord_application_setting_category', 'contains', ApplicationSettings.CATEGORY_GLOBAL_ID ],
            //    'or',
            //    [ 'custrecord_application_setting_category', 'contains', sApplicationCategory ] 
                //];
            // **************************************
            
             var sr = nlapiSearchRecord(ApplicationSettings.NS_RECORD_ID, 'customsearch_mp_custom_app_settings_srch', new nlobjSearchFilter('custrecord_application_setting_category', null, 'contains', sApplicationCategory ))|| [];
            this.log('AUDIT', "ApplicationSettings.load", "sr length: " +sr.length);
             for (var i = 0; i < sr.length; i++) {
                 var key = sr[i].getValue('custrecord_application_setting_key');
                 self.applicationSettings[key] = sr[i].getValue('custrecord_application_setting_value');
             }
             this.log('AUDIT', "ApplicationSettings.load", "self.applicationSettings.size: " +self.applicationSettings.size);
             
            
        } else {
            this.log('AUDIT', "ApplicationSettings.load", "NetSuite Search API not avialable.");
        }        
    } 
    catch (e) 
    {
        this.log('ERROR',"ApplicationSettings.load", e.message);
    }
};

// TODO Fix the storage feature.  currently broken due to results ordering.
ApplicationSettings.prototype.store = function() {
    if (typeof nlapiSearchRecord !== "undefined") {
        // We only want to update application specific keys
        var filters = [
            new nlobjSearchFilter('custrecord_application_setting_category', null, 'contains',  this.getSetting(ApplicationSettings.APP_CATEGORY_KEY) ),
            new nlobjSearchFilter('custrecord_application_setting_key', null, 'contains', this.updatedKeys)
        ];
        var columns = [
            new nlobjSearchColumn('custrecord_application_setting_category'),
            new nlobjSearchColumn('custrecord_application_setting_key'),
            new nlobjSearchColumn('custrecord_application_setting_value')
        ];
        // need to set order because if you don't its random - contrary to the API spec.  <sigh>
        columns[0].setSort(false);
        
        var oOnDiskView = {};
        var results = nlapiSearchRecord(ApplicationSettings.NS_RECORD_ID, null, filters, columns) || [];
        // depends on result order being #global followed by application
        for (var i = 0; i < results.length; i++) {
            var result = results[i];
            var key = result.getValue('custrecord_application_setting_key');
            oOnDiskView[key] = { "id":result.getId(), 
                                 "value":result.getValue('custrecord_application_setting_value'), 
                                 "key":key, 
                                 "category":result.getValue('custrecord_application_setting_category')};
        }
        this.tmpOnDiskView = oOnDiskView;
        
        // update existing records
        var oMatch, oNewRec, nRecId;
        this.log('DEBUG', "updatedKeys", JSON.stringify(this.updatedKeys));
        for (var i = this.updatedKeys.length - 1; i >= 0; i--) {
            oMatch = oOnDiskView[this.updatedKeys[i]];
            this.log('DEBUG', "oMatch "+i, JSON.stringify(oMatch));

            if ( oMatch && oMatch.category != ApplicationSettings.CATEGORY_GLOBAL_ID ) {
                // If #global was updated it will be created in the application space
                nlapiSubmitField(ApplicationSettings.NS_RECORD_ID, oMatch.id, 'custrecord_application_setting_value', this.getSetting(this.updatedKeys[i]));
                this.log('DEBUG', "ApplicationSettings.store", "Updated "+oMatch.category+"."+oMatch.key+" to "+this.getSetting(this.updatedKeys[i]));
            } else {
                // Create a new record in the application category
                oNewRec = nlapiCreateRecord(ApplicationSettings.NS_RECORD_ID);
                oNewRec.setFieldValue('custrecord_application_setting_category', this.applicationSettings[ApplicationSettings.APP_CATEGORY_KEY] );
                oNewRec.setFieldValue('custrecord_application_setting_key',      this.updatedKeys[i] );
                oNewRec.setFieldValue('custrecord_application_setting_value',    this.applicationSettings[this.updatedKeys[i]] );
                nRecId = nlapiSubmitRecord(oNewRec);
                this.log('DEBUG', "ApplicationSettings.store", "Created "+ApplicationSettings.APP_CATEGORY_KEY+
                                                                      "."+this.updatedKeys[i]+
                                                                      " to "+this.getSetting(this.updatedKeys[i])+" ID:"+nRecId);
            }
        }
        this.updatedKeys = []; // reset the list 
    } else {
        this.log('ERROR', "ApplicationSettings.store", "NetSuite Search API not avialable. Nothing stored.");
    }
};

ApplicationSettings.prototype.length = function() {
    return this.applicationSettings.length || 0;
};

ApplicationSettings.prototype.size = function() {
    var size = 0, key;
    for (key in this.applicationSettings) {
        if (this.applicationSettings.hasOwnProperty(key)) size++;
    }
    return size;
};

ApplicationSettings.prototype.getSetting = function(sKey) {
    return this.applicationSettings[sKey];
};

ApplicationSettings.prototype.setSetting = function(sKey, sValue) {
    this.recordKeyUpdate(sKey);
    return this.applicationSettings[sKey] = sValue;
};

/** 
 *  Convenience method for reconsituting a complex data structure 
 *
 *  @param {String} sKey The key of the value to retrieve
 */
ApplicationSettings.prototype.getJsonSetting = function(sKey) {
    return JSON.parse(this.applicationSettings[sKey]);
};

/** 
 *  Convenience method for storing a data structure as JSON
 *
 *  @param {String} sKey The storage key for the value
 *  @param {Object} oValue The object to store as a JSON string
 */
ApplicationSettings.prototype.setJsonSetting = function(sKey, oValue) {
    this.recordKeyUpdate(sKey);
    return this.applicationSettings[sKey] = JSON.stringify(oValue);
};

/** 
 *  Add a key to the list of those that have been updated
 *  Used to manage the .store operation.  Keys are only stored once
 *  Call from ALL setters 
 * 
 *  @param {String} sKey A key value to add to the list 
 */
ApplicationSettings.prototype.recordKeyUpdate = function(sKey) {
    if (-1 == this.updatedKeys.indexOf(sKey)) { this.updatedKeys.push(sKey); }
};

/**
 * Log method to log using Netsuite logging or Console logging,
 * whatever is available.
 *
 * @param {String} level   Netsuite Log level.
 * @param {String} source  The method that is logging.
 * @param {String} message The log message.
 */
ApplicationSettings.prototype.log = function(level, source, message) {

    if (typeof nlapiLogExecution !== 'undefined') {
        nlapiLogExecution(level, source, message);
    }

    if (typeof console !== 'undefined') {
        console.log("%s: %s - %s", level, source, message);
    }
};

/**
 * Return an object containing the values of two input objects
 *
 * @param {Object} a An object
 * @param {Object} b Another object
 * @return {Object} Combined object
 */
ApplicationSettings.prototype.extend = function(a, b) {
    for (var key in b) {
        if (b.hasOwnProperty(key)) {
            a[key] = b[key];
        }
    }
    return a;
};





/*   Manual testing

var app = new ApplicationSettings({"application":"foo"}); 
app.setSetting('new','local value');
app.setSetting('a','change local value');
app.setSetting('d','override global d with new local');
app.store();

*/


