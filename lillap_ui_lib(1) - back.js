// Color data sample
// { 
//  "0":{
//      "id":"203",
//      "name":"Apricot",
//      "rgb":"fab788",
//      "url":"http://www.lillap.com/site/swatch/"
//      },
//  "1":{ ... },
//  "length":2
// }
var NONE_SELECTED = 'None Selected';
var count = 0;      
var clickcount = 0;     
var triggerAgain = 0;

if (typeof(window.lillap) === "undefined") window.lillap = {};

jQuery(document).ready(function(){

    //events for show more colors available
  
    if (window.matchMedia('(max-width: 768px)').matches)
    {
     jQuery('.swatchboxes',this).css('display','block'); 
    }

    else{
     jQuery( ".item-cell").bind({
      mouseenter: function() {
       jQuery(".swatchboxes",this).css('display','block');  
      },
      mouseleave: function() {
        jQuery(".swatchboxes",this).css('display','none');  
      }
     });
    } 
    jQuery(".item-cell").click(function(){

        var selected = jQuery('.swatchBox.selected');
      
    })    
    // Main Nav workaround for NS issue
    if (document.location.host.match('lillap'))
    {            
        try{
            //jQuery('#left_menu_contents .portletHandle tr:first-child').hide();
            //jQuery('#left_nav_table tbody tr.noprint').first().hide(); 
            jQuery('#left_menu_contents a[href="/Home"]').attr('href','/Welcome');  
            jQuery('a[href="/Home/Origami"]').attr('href','/Home/Origami/TWRP');
        }      
        catch(e) { /* TODO - handle jQuery lookkup failures */ }
    }
    initSearch();
    
    if (jQuery('#itemContent').length > 0) 
    {
        setupOptionsChooser();
         var itemType = jQuery('#itemtype').val();
        if ( window.lillap.bMatrixEmpty ) { jQuery('#addToCart').hide(); }  else {jQuery('.ddOptions').show();
       // console.log('show ddoptions')
    }; 
        itemAltImageSetup();
        salePriceCustomizations();
        
        if(itemType == "GiftCert"){
            var itemInternalId = jQuery('#iteminternalid').val();
            jQuery(".crumb"). removeAttr("href");
            jQuery(".accordion").css("display","none");
            jQuery("#custcol_size_"+itemInternalId).empty();
            jQuery("#custcol_size_"+itemInternalId+"_lbl").empty();
            jQuery("#custcol_coloroption_"+itemInternalId+"_lbl").empty();
            jQuery("#custcol_coloroption_"+itemInternalId).empty();
            jQuery("#dd-listprice").css("display","none");
            jQuery("#GIFTCERTFROM").css({"height": "30px", "width": "270px", "border-color": "#d9d1cc"});
            jQuery("#GIFTCERTRECIPIENTNAME").css({"height": "30px", "width": "270px", "border-color": "#d9d1cc"});
            jQuery("#GIFTCERTRECIPIENTEMAIL").css({"height": "30px", "width": "270px", "border-color": "#d9d1cc"});
            jQuery("#custcol_amount_formattedValue").css({"height": "30px", "width": "100px", "border-color": "#d9d1cc"});
            jQuery(".dd-sku").css({"font-size": "1.750rem", "margin": "5px 0 20px","line-height": "1.1","font-weight": "300","text-transform": "uppercase", "color": "#8d827a"});
            jQuery("#GIFTCERTRECIPIENTNAME_fs_lbl").css({"position": "relative", "top": "5px"});
            jQuery("#GIFTCERTRECIPIENTNAME_fs").css({"position": "relative", "top": "5px"});
            jQuery("#GIFTCERTRECIPIENTEMAIL_fs_lbl").css({"position": "relative", "top": "10px"});
            jQuery("#GIFTCERTRECIPIENTEMAIL_fs").css({"position": "relative", "top": "10px"});
            jQuery("#GIFTCERTMESSAGE_fs_lbl").css({"position": "relative", "top": "15px"});
            jQuery("#GIFTCERTMESSAGE_fs").css({"position": "relative", "top": "15px"});
            jQuery("#custcol_amount_57976_lbl").css({"position": "relative", "bottom": "10px"});
            jQuery("#custcol_amount_57976").css({"position": "relative", "bottom": "10px"});
            jQuery(".smalltextnolink").css("font-size","13px");
            jQuery("#addToCart input[type='button']").css({"margin-left": "135px", "margin-top": "20px"});
            
            //jQuery(".inputreq").addClass("container");
            var screenWidth= jQuery(window).width();
                     
            if (screenWidth <= 767) {

                jQuery("#itemSocials").css({"position": "relative", "right": "-135px","top":"-40px"});
                //Do Something
               jQuery("#div__body table.atc td").css({"display": "block","height": "auto"});
            }
            if (screenWidth >= 968) {
                jQuery("#itemSocials").css({"position": "relative", "right": "-135px","top":"-40px"});
            }
           if (screenWidth>=768 && screenWidth<=967) {

                //Do Something
               jQuery("#div__body table.atc td").css({"display": "block","height": "auto"});
                jQuery("#itemSocials").css({"position": "relative", "right": "-135px","top":"-890px"});
            }
        }
         jQuery('#sendData').click(function(e){     
                e.preventDefault();     
                var hasErrors = false;          
                // Validate required fields     
                jQuery('#sendData').css('top','3px');       
                jQuery('.hint_1 .form-errors').css('height','6px');     
                jQuery('.pText').css({'top':'8px','position':'relative'});      
                jQuery('.hint_1').css('height','556px');        
                /*jQuery('.colorerror').css({'top':'32px','position':'relative'});*/        
                jQuery('.hint_1').parents().css('overflow','hidden  !important');       
                var sizetext = jQuery('#curSize1').text();      
                var colortext = jQuery('#curColor1').text();        
                if (sizetext == 'None Selected') {      
                    hasErrors = true;       
                    jQuery('.sizeerror').css('display','block');        
                            
                }       
                if (colortext == 'None Selected') {     
                    hasErrors = true;                       
                    jQuery('.colorerror').css('display','block');       
                    jQuery('.testdiv').css('left','0px');       
                }       
                jQuery('.hint_1 .signval').each( function(index, value) {       
                    var inputs = jQuery(this),      
                    inputs_val = jQuery.trim(inputs.val());     
                    //console.log('index '+index);      
                    if ( inputs_val.length === 0 || inputs_val === '' ) {       
                        hasErrors = true;       
                        if (index == 0) {jQuery('.nameform').css('display','block')}else if (index == 1) {jQuery('.fromform').css('display','block')}else if (index == 2) {jQuery('.toform').css('display','block')}        
                        inputs.next('.form-errors').css('display','block');     
                    } else if (inputs.attr('type') === 'email') {       
                        var emailAddr = inputs.val(),       
                        emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;       
                        if (!emailReg.test(emailAddr)) {        
                            hasErrors = true;       
                            if (index == 1) {jQuery('.fromform').html('Please enter a valid email address').css('display','block')}else if (index == 2) {jQuery('.toform').html('Please enter a valid email address').css('display','block')}       
            
                            /*inputs.next('.form-errors').html('Please enter a valid email address').css('display','block');*/      
                        }       
                        else{       
                            if (index == 1) {       
                                jQuery('.fromform').css('display','none');      
                            }       
                            else if (index == 2){       
                                jQuery('.toform').css('display','none');        
                            }       
                        }       
                    }       
                    else{       
                                
                        if (index == 0) {jQuery('.nameform').css('display','none')}else if (index == 1) {jQuery('.fromform').css('display','none')}else if (index == 2) {jQuery('.toform').css('display','none')}       
                    }       
                });     
                if (!hasErrors) {       
                            
                    var fromEmail = jQuery('#hint_from_email').val();       
                    var toEmail = jQuery('#hint_email').val();      
                    var name = jQuery('#input_firstname').val();        
                    var subject = jQuery('#hint_subject').val();        
                    var imgurl = jQuery('#sBaseName').val();        
                    var item_name = jQuery('.item_name').text();        
                    var bgcolor = jQuery('#bgcolor').val();     
                    var bgcolorid = jQuery('#bgcolorid').val();     
                    var bgsize = jQuery('#bgsize').val();       
                    var bgsizeid = jQuery('#bgsizeid').val();       
                    var personalizeText = jQuery('.textarea').val().replace(/(?:\r\n|\r|\n)/g, '<br />');       
                    //console.log('imgurl '+imgurl);        
                    var imageurl = '/items/item/'+imgurl+'-ITEM.jpg';       
                    var presenturl = window.location.href;
                    jQuery.ajax({       
                      url: '/app/site/hosting/scriptlet.nl?script=955&deploy=1&compid=631629_SB2&h=e17f429b3508c1139b91'+'&fromEmail='+fromEmail+'&toEmail='+toEmail+'&name='+name+'&subject='+subject+'&imageurl='+imageurl+'&item_name='+item_name+'&bgcolor='+bgcolor+'&bgsize='+bgsize+'&presenturl='+presenturl+'&bgcolorid='+bgcolorid+'&bgsizeid='+bgsizeid+'&personalizeText='+personalizeText,     
                      type: 'GET'       
                    });     
                    jQuery('.hint_1').css('display','none');        
                    jQuery('.hint_2').css('display','block');       
                }       
            
            });
        var productcategory = jQuery('#productcategory').val();
        if (productcategory=='Pant'||productcategory=='Skirt'||productcategory=='Cardigan'||productcategory=='Robe') 
            jQuery('.bottom_sizechart').css('display','block');
        else
            jQuery('.sizeChart').css('display','block');
        


        jQuery('.details-content').each(function(i, obj) {
             var value=jQuery(this).html();
             value=value.trim();
             if(value==null||value=="")
                 jQuery(this).css('display','none');
            else
                jQuery(this).css('display','list-item');
               
        });
        var product_care_instructions = jQuery('.productCareInstruc').html();
        if (product_care_instructions == '') {
            jQuery('.productCareInstruc').text('Hand Wash');
        }
        var pdp = window.location.href;             
            var sizeid=pdp.indexOf("sizeid");       
            var parsedsizeid = '';                  
            if(sizeid>-1){              
                var loginURL = window.location.search.substring(1);
                var variable = loginURL.split('&');     
                for (var i = 0; i < variable.length; i++) {     
                    var names = variable[i].split('-');     
                    for (var j=0;j<names.length;j++) {      
                        if (names[j] == 'sizeid'){      
                            parsedsizeid=names[j+1];        
                            ddSizeSelected (parsedsizeid);       
                        }       
                    }       
                }       
            } 

        var sizemodel = jQuery('#sizemodel').val();
        if (sizemodel != '') {
            jQuery('.sizeWomen').css('display','list-item');
        }
        else
            jQuery('.sizeWomen').css('display','none');
    }
    var currenturl =window.location.href;
    var thanks=currenturl.search("thankyou");
    
    jQuery('#category_image_container img').each(function(idx,elem) {
        try {
            var aStyle = jQuery(this).attr('title').match('(^[a-zA-Z0-9]+).*');
          if (aStyle && aStyle.length > 1)
          {
            var dst = location.protocol + "//" + location.host + "/" + aStyle[1];
            jQuery(this).parent().attr('href', dst);
          }
   
        } catch(e) { /* we do nothing if error, URL unchanged */ };
    });
    if (jQuery('#itemContent').length > 0) 
    {
        var sBaseName = '';
      var pdpurl = window.location.href;
        var n = pdpurl.lastIndexOf("-");
       var parsedcolorid= pdpurl.substring(n+1)
        ddSelectSwatch(parsedcolorid);
        if(window.lillap.colorSelect===0){
            ddSelectAltImage(findAltImgElementFromColor(parsedcolorid));
            //console.log('sBaseName '+sBaseName);
        }
    }
    
    // TABS
    // when page first loads
    var tabContents = jQuery('.tab_content').hide(),
    tabs = jQuery('ul.tabs li');

    tabs.first().addClass('active').show();
    tabContents.first().show();

    // when tab is clicked
    tabs.click(function() {
      var $this = jQuery(this),  
      activeTab = $this.find('a').attr('href');

      if(!$this.hasClass('active'))
      {
        $this.addClass('active').siblings().removeClass('active');
        tabContents.hide().filter(activeTab).fadeIn();
      }

      return false;
    });

    // Suggest IE9 user not use compatibility mode
    var sUA = navigator.userAgent;
    if (sUA.indexOf('Trident/5.0') > 0) {
        if (sUA.indexOf('MSIE 9.0') < 0) { 
            alert('You are using IE9 in Compatibility mode, which is NOT supported by this site.  Please switch to standards mode by clicking on the broken document icon in your browser address bar.   Thanks!');
        };
    };
    
    // jQuery('#category_image_container img').click(function(oEvt) {
    //   var imgFname = jQuery(this).attr('title');
    //   var aStyle = imgFname.match('([a-zA-Z0-9]+)[ \._]');
    //   if (aStyle && aStyle.length > 1)
    //   {
    //     var dst = location.protocol + "//" + location.host + "/" + aStyle[1];
    //     document.location.href = dst;
    //   }
    // });
    // splash image js inline in html file
});

// For category and search lists 
function itemCellHover () {
    jQuery('.item-cell').hover( function(oEvt) { jQuery(this).find('.item-info').fadeIn(300); }, function(oEvt) {  jQuery(this).find('.item-info').fadeOut(200); } );
}
// *********** Search Support *******

function initSearch () {
    jQuery('#searchportlettag').each(function(idx, elem) { jQuery('#search-proxy').css('display','block'); });
    jQuery('#search-proxy').submit(function() {
      jQuery('input[name="search"]').val(jQuery('#search-proxy-input').val());
      var srchBtn = jQuery('#go');
      if (srchBtn.length > 0) { srchBtn.click();}
      else { 
        // inside search results page
        jQuery('#searchbutton').click(); 
      };
      return false;
    });
}

// ************  Options Chooser Behaviour **********

function setupOptionsChooser () {
    
    try {
        var w = window;
        if (typeof(w.lillap) === "undefined") w.lillap = {};

        //Grab the colors and sizes from the dropdowns
        var aAvailableSizeIds = getSelectValueArray(document.getElementById('custcol_size')).slice(1);
        var aAvailableColorIds = getSelectValueArray(document.getElementById('custcol_coloroption')).slice(1);
        
        // Workaround for NS issue (case# 1269370) - item displayed in site but no stock

        var itemType = jQuery('#itemtype').val();
        w.lillap.bMatrixEmpty = (aAvailableSizeIds != null && aAvailableColorIds != null && aAvailableSizeIds.length == 0 && aAvailableColorIds.length == 0 && itemType != GiftCert);

        var nItemId = document.getElementById('buyid').value;
        var aOutOfStock = eval('Item'+nItemId+'_avail');
        var w = window;
        
        w.lillap.aOutOfStock = aOutOfStock;  // for use in availability checks
        w.lillap.aAvailableSizeIds = aAvailableSizeIds;
        w.lillap.aAvailableColorIds = aAvailableColorIds;
        w.lillap.aColorIdInStock = new Array();

       window.lillap.availableSizes=0;
       window.lillap.availableColors=0;
       window.lillap.colorCount=0;
       window.lillap.colorCountId=0;
      
      window.lillap.singleSize="";
      window.lillap.singleColor="";
      window.lillap.sizeDim=new Array();
      window.lillap.colorSelect=0;
      window.lillap.colorRefresh=new Array();
      window.lillap.mainImageLoad=0;
     
      
        // inject containers for option boxes as a new row with divs 
        var oOptionContext = jQuery('#nsOptions');
        oOptionContext.before('<tr><td><div class="ddOptions"><p>Size:<span id="curSize">None Selected</span></p><div id="sizeContainer"></div><p>Color:<span id="curColor">None Selected</span></p><div id="swatchContainer"></div><div id="swatchPopup"><div id="swatchImg"></div><div id="swatchInfo"><p/></div></div></div><div id="optionsMessage"></div></td></tr>');
        // TODO - why should this be necessary?  Its set in CSS file
        jQuery('.ddOptions').css('position','relative');

        if (w.lillap.bMatrixEmpty && itemType != 'GiftCert') 
        {
            jQuery('#optionsMessage').html('Presently Unavailable');
            jQuery('#optionsMessage1').html('Presently Unavailable');
            jQuery('.ddOptions').hide();
        }
        var sSizeHTML = "";
        var sSizeHTMLpop = "";
        for (var i=0; i < aAvailableSizeIds.length; i++) {
            sSizeHTML += buildSizeHtml( aAvailableSizeIds[i] );
        };
        for (var i=0; i < aAvailableSizeIds.length; i++) {      
            sSizeHTMLpop += buildSizeHtmlpop( aAvailableSizeIds[i] );       
        };
        jQuery('#sizeContainer').append(sSizeHTML);
        jQuery('.popupsize').append(sSizeHTMLpop);
        // jQuery('.sizeButton').each(function(idx, el){
        //  jQuery(el).data('size', jQuery(el).attr('id').split('-')[1]);
        // });
        jQuery('.sizeBox').click( ddSizeSelected );

         if(w.lillap.aAvailableSizeIds.length==1)
         {
            var selectedId= "#size-"+aAvailableSizeIds[0];
            var selectedIdpop= "#sizepop-"+aAvailableSizeIds[0];
            
            jQuery(selectedId).trigger( "click" );
            jQuery(selectedIdpop).trigger( "click" );
            

         }         
        var sColorHTML = "";
        var nOutCount = 0;
        
        for (var i=0; i < aAvailableColorIds.length; i++) {
            nOutCount = 0;
            for (var j=0; j < aAvailableSizeIds.length; j++) {
                if(aOutOfStock[aAvailableSizeIds[j]+':'+aAvailableColorIds[i]]) {
                    nOutCount++;
                }
            };
            if(nOutCount < aAvailableSizeIds.length) {
                w.lillap.aColorIdInStock[aAvailableColorIds[i]] = true;
                sColorHTML += buildColorHtml( aAvailableColorIds[i] ); // color in stock
            } 
        };

        jQuery('#swatchContainer').append(sColorHTML).mouseleave( ddHideSwatch );
        
        // New system get the color information from #parent_color_map
        // Defined in the !LP RES item drilldown template
        var parent_color_map = JSON.parse(jQuery('#parent_color_map').val());
        var url = '/items/swatch/';
        var colorbox, colorbox_class, rgb, sName;
        
        for (var i = 0; i < aAvailableColorIds.length; i++) {
            
            w.lillap.colorData = parent_color_map;
            colorbox = jQuery('#colorbox-'+aAvailableColorIds[i]);
            
            //Check that the color exists in the parent color map, otherwise, write the name
            if (parent_color_map[aAvailableColorIds[i]]) {
                
                colorbox_class = parent_color_map[aAvailableColorIds[i]].abbreviation ? parent_color_map[aAvailableColorIds[i]].abbreviation : 'NNN';
            
                parent_color_map[aAvailableColorIds[i]].id = aAvailableColorIds[i];
            
                if (parent_color_map[aAvailableColorIds[i]].swatch.length > 0){
                    parent_color_map[aAvailableColorIds[i]].url = url + parent_color_map[aAvailableColorIds[i]].swatch ;
                    colorbox.html('<img src="'+url+parent_color_map[aAvailableColorIds[i]].swatch+'" data-pin-nopin="true">' );
                }
            
                rgb = parent_color_map[aAvailableColorIds[i]].rgb ? parent_color_map[aAvailableColorIds[i]].rgb : 'FFF';
                sName = parent_color_map[aAvailableColorIds[i]].name ? parent_color_map[aAvailableColorIds[i]].name : 'Color Name';
            
                colorbox.data(parent_color_map[aAvailableColorIds[i]]);
            
            } else {
                
                //Do something
                colorbox_class ='';
                rgb = '';
                sName = 'Color Name';
                
                colorbox.data({"id": aAvailableColorIds[i], "name":"Color Name","rgb":"FFF","swatch":"","abbreviation":"NNN","source":"w"});
            }
            
            
            colorbox.addClass(colorbox_class);
            colorbox.css('background-color','#'+rgb);
            colorbox.attr('title', sName);
            colorbox.parent().mouseenter( ddShowSwatch );
            
            colorbox.click( ddSwatchClicked );
              // was on parent

         
           window.lillap.sizeDim.length=0;
          
           

        };
        
    } catch(e) { if (typeof(console) === "object") console.log("setupOptionsChooser exception:"+e);};
    


}
function buildColorHtml (nColorId) {
    var html = "<div class='swatchBox swatchBox12'><a href='javascript: void {};' class='swatchLink'><div class='swatchColor' id='colorbox-"+
        nColorId+"'>&nbsp;</div></a></div>";
        window.lillap.colorCount++;
        window.lillap.colorRefresh[window.lillap.colorCount-1]=nColorId;

        if(window.lillap.colorCount==1)
        {
            window.lillap.colorCountId=nColorId;
        }
      
        
    return html;
};

function buildSizeHtml (nSizeId) {
    //var aSizeLbl = ['Error','XS','S','M','L','XL','OS','PS','ML'];

    var aSizeLbl = ['Error','XS','S','M','L','XL','OS','PS','ML','S/M','M/L','00','02','04','06','08','10','12','14'];
    
    var html = "<div class='sizeBox'><a href='javascript: void {};' class='sizeLink'><div class='sizeButton' id='size-"+nSizeId+"'>"+
        aSizeLbl[nSizeId]+"</div></a></div>";
    return html;
};

function ddShowSwatch ( oEvt ) {
   
    var rgb = jQuery(oEvt.target).data('rgb') ? jQuery(oEvt.target).data('rgb') : 'FFF';
    var swatchimg = jQuery('#swatchImg');
    var swatchimg1= jQuery('#swatchImg1');
    var tgt = jQuery(oEvt.target);
   
    swatchimg.css('background-color', '#'+rgb);
    swatchimg1.css('background-color', '#'+rgb);
    // TODO there must be a better way to do this with bubbling...
    if (oEvt.target.nodeName == 'IMG') tgt = tgt.parents('.swatchColor');
    if (tgt.data('url')){
        
        swatchimg.html('<img src="'+tgt.data('url')+'" data-pin-nopin="true">' );
        swatchimg1.html('<img src="'+tgt.data('url')+'" data-pin-nopin="true">' );
    }
    else {
        swatchimg.html('');
        swatchimg1.html('');
    }
    jQuery('#swatchInfo p').html(tgt.data('name'));
     jQuery('#swatchInfo1 p').html(tgt.data('name'));
    var selectId="#colorbox-"+tgt.data('id');
    
    if(jQuery(selectId).hasClass("unavailable"))
    {
      jQuery('#swatchPopup').hide();
      jQuery('#swatchPopup1').hide();
    }
    else{
    jQuery('#swatchPopup').show();
    jQuery('#swatchPopup1').show();
        }
};

function ddHideSwatch ( oEvt ) {
    jQuery('#swatchPopup').hide();
    jQuery('#swatchPopup1').hide();
};

function deselectSwatch () {
    var selected = jQuery('.swatchBox12.selected');
    var selector = jQuery('.swatchBox1.selected');
    var colorSelElem = document.getElementById('custcol_coloroption');
    if (selected) selected.removeClass('selected');
    jQuery('#curColor').html(NONE_SELECTED);
    setSelectValue(colorSelElem, ''); 
};

function ddSwatchClicked ( oEvt ) {
   window.lillap.userClick=0;
    
   
    var oTgt = jQuery(oEvt.target);
    var nClrId = getColorId(oEvt.target);
    window.lillap.availableSizes=0;
    if(oEvt.hasOwnProperty('originalEvent'))
    {
    
    window.lillap.userClick++;
    window.lillap.colorSelect=0;
    
} 
    else{
           

        }
    if (oTgt.hasClass('swatchColor')) {
        
            
                 ddSelectSwatch(nClrId);
         
    } 
    else
    {
        nClrId = getColorId(oTgt.parent());
        ddSelectSwatch(nClrId); 
        // on img, get parent swatchColor div
        
    }   
    if(window.lillap.colorSelect===0){
       
    ddSelectAltImage(findAltImgElementFromColor(nClrId));
    
    }
}



function ddSelectSwatch ( sColorId, bIndirect ) {
    
     
    var jElem = jQuery('#colorbox-'+sColorId),
        sName = '',
        sAbbr = '';
    sName = jElem.data('name');     
    sAbbr = jElem.data('abbr'); 
    
   window.lillap.colorElemName=sName;
    if ( ! ddIsColorAvailable(sColorId) ) {
        deselectSize();
        ddSetColorAffordances ();
        
       
    }
    var colorSelElem = document.getElementById('custcol_coloroption');
   
    var selected = jElem.parents('div.swatchBox12:first');
    
      // Which was clicked
    var cur_selection = jQuery('.swatchBox12.selected');    
    
                    // What was selected
    var cur_color = getColorId(jQuery('.swatchColor', cur_selection));
     
    if (bIndirect && (sColorId == cur_color))
    {
        // leave currently selected color selected
    }
    else {
        if (selected && selected.length > 0 && cur_selection && cur_selection.length > 0 && selected[0] === cur_selection[0] ) {
            deselectSwatch();
            jQuery('#curColor').html(NONE_SELECTED);
            selected.removeClass('selected'); //.css('border-color','transparent');
            setSelectValue(colorSelElem, ''); 
            
            
            window.lillap.mainImageLoad=1;
            window.lillap.deselectSwatch=1;

            for(var refresh=0;refresh<window.lillap.colorRefresh.length;refresh++)
            {
               
                var selectId="#colorbox-"+window.lillap.colorRefresh[refresh];
                jQuery(selectId).removeClass("unavailable");
                jQuery(selectId).attr('title','');
            }
          deselectSize();

        } 
        else // new selection
        {   window.lillap.deselectSwatch=0;
            deselectSwatch();
            window.lillap.mainImageLoad=0;
            jQuery('#curColor').html(sName);
            selected.addClass('selected'); // .css('border-color','black');
            setSelectValue(colorSelElem, sColorId); 
        }
    }
    ddSetSizeAffordances ();
};

// From the product selector
function selectedColorId () {
    var sId = jQuery('.swatchBox.selected .swatchColor').attr('id'),
        sIdRetval = "";
    if (sId) sIdRetval = sId.split('-')[1];
    
    return sIdRetval;
}
function selectedSizeId () {
    var sId = jQuery('.sizeBox.selected .sizeButton').attr('id'),
        sIdRetval = "";
    if (sId) sIdRetval = sId.split('-')[1];
    return sIdRetval;
}

function getSizeId (oElem) {
    if (oElem && jQuery(oElem).attr('id')) return jQuery(oElem).attr('id').split('-')[1];
    return "";
}
function getColorId (oElem) {
    if (oElem && jQuery(oElem).attr('id')) return jQuery(oElem).attr('id').split('-')[1];
    return "";
}
function getColorIdFromAbbr (sAbbr) {
    var nColorId = "";
    jQuery('.swatchColor').each(function(idx, oElem){
        if (sAbbr == jQuery(oElem).data('abbr')) {
            nColorId = getColorId(oElem);
            return false; // stop loop
        }
    });
    return nColorId;
}


function ddSetSizeAffordances () {
    var selectedId;
    window.lillap.availableSizes=0;
    

    jQuery('#sizeContainer .sizeButton').each(function(idx, oElem){
        var oBtn = jQuery(oElem);
        var unavailableConstant=0;
        var oSizeBox = oBtn.parents('div.sizeBox:first');
        
        if (ddIsSizeAvailable(getSizeId(oElem), selectedColorId() )) {
            
            
          
           if(oBtn.hasClass("notavailable"))
           {
            oBtn.attr("title","Unavailable");
           }

            window.lillap.availableSizes++;
           oBtn.attr("title","").removeClass('unavailable');  
           selectedId= "#size-"+getSizeId(oElem);
              

        } 
        else {
           
            oBtn.attr('title', 'Unavailable');
           
            if(oBtn.hasClass("notavailable"))
            {

            }
            else{
            oBtn.attr("title","Unavailable For "+window.lillap.colorElemName).addClass('unavailable');
            
                }
        }
    });

    if(window.lillap.availableSizes==1){
       
        var singleSizeTestId="";
        
        
        var cur_sel=jQuery('.sizeBox.selected');
       
       
         if(cur_sel.length==0)
         {
           
         }
         else{
            
          var singleSizeTest=document.getElementById('custcol_size').value;
        
        singleSizeTestId="#size-"+singleSizeTest;
           }
           
           if(singleSizeTestId==""||singleSizeTestId!=selectedId){
           window.lillap.singleSize=selectedId;
           
           if(window.lillap.deselectSwatch===0){
          jQuery(selectedId).trigger( "click" );}
          
          

  }
         
          window.lillap.availableSizes=0;
          


    }
    else{
        if(window.lillap.singleSize!="")
        {   
            deselectSize();
             window.lillap.singleSize="";
        }
        else{
        window.lillap.availableSizes=0;
    }
    }
    
};

function deselectSize () {
    var selected = jQuery('.sizeBox.selected');
    var sizeSelElem = document.getElementById('custcol_size');
    if (selected) selected.removeClass('selected');
    jQuery('#curSize').html(NONE_SELECTED);
    jQuery('#curSize1').html(NONE_SELECTED);
    setSelectValue(sizeSelElem, ''); 

};
function ddSizeSelected ( oEvt ) {
    
    window.lillap.userClick=0;
    window.lillap.availableColors=0;

   if(oEvt.hasOwnProperty('originalEvent'))
    {
      window.lillap.colorSelect=0;
    }
    else
    {
        
    } 
    
  
     if ( ! ddIsSizeAvailable(jQuery(oEvt.target).attr('id').split('-')[1]) ) {
        
        deselectSwatch();
        ddSetSizeAffordances();  
       
    }
    var selected = jQuery(oEvt.target).parents('div.sizeBox:first');
    var cur_selection = jQuery('.sizeBox.selected');

    var sizeSelElem = document.getElementById('custcol_size');
    if (selected && selected.length > 0 && cur_selection && cur_selection.length > 0 && selected[0] === cur_selection[0] ) {
        deselectSize();
         
          window.lillap.deselectSize=1;
        jQuery('#curSize').html(NONE_SELECTED);
        setSelectValue(sizeSelElem, ''); 
        selected.removeClass('selected');
         // css('border-color','transparent');
         var x= window.lillap.aAvailableSizeIds.filter(function(n) {
                                          return window.lillap.sizeDim.indexOf(n) == -1;
         

                                    });

        var col_cur_sel=jQuery('.swatchBox.selected');  //If a color is already selected donot refresh sizes
        
        for(var refresh=0;refresh<x.length;refresh++)
        {
            var selectId="#size-"+x[refresh];
            
            jQuery(selectId).attr("title","").removeClass("unavailable");
             var selectIdpop="#sizepop-"+x[refresh];                    
            jQuery(selectIdpop).attr("title","").removeClass("unavailable");
        }
        deselectSwatch();
        var bgsize = jQuery("#sizepop-"+x[refresh]).html(); 
        jQuery('#mainImage').attr('src',window.lillap.mainImage);
        jQuery('#ddZoomImageDiv img').attr('src',window.lillap.zoomImage);
        
        jQuery('.item-alt-wrap').removeClass('selected');   
        jQuery('.item-alt-wrap',window.lillap.selectElem).addClass('selected');
       

    } 
    else // new selection
    {   
        deselectSize();
        window.lillap.deselectSize=0;
        var nSize = getSizeId(oEvt.target); 
        jQuery('#curSize').html(getSelectTextForValue(sizeSelElem, nSize));
        var sizeselect = jQuery('#curSize1').html(getSelectTextForValue(sizeSelElem, nSize));
        window.lillap.sizeElemName=getSelectTextForValue(sizeSelElem, nSize);
        setSelectValue(sizeSelElem, nSize); 
        selected.addClass('selected'); // css('border-color','black');
        var bgsize = jQuery('#sizepop-'+nSize).html();              
            /*var finalsize = bgsize.find('div').html();*/                 
            //console.log('bgsize '+bgsize);                   
            jQuery('#bgsize').val(bgsize);
            jQuery('#bgsizeid').val(nSize);           
           
    }

    
     
    ddSetColorAffordances();
};

function ddSetColorAffordances () {
    
   
    window.lillap.availableColors=0;
    var selectedId;
    jQuery('#swatchContainer .swatchColor').each(function(idx, oElem){
        var oBtn = jQuery(oElem);
        var oColorBox = oBtn.parents('div.swatchBox:first');
        if (ddIsColorAvailable(getColorId(oElem), selectedSizeId() )) {
            
            oBtn.attr('title', '').removeClass('unavailable');
           
             window.lillap.availableColors++;
            
           
            selectedId= "#colorbox-"+getColorId(oElem);
             
             
            
            
        } 
        else {
            oBtn.attr('title', 'Unavailable For '+window.lillap.sizeElemName).addClass('unavailable');
        }
    });
    jQuery('.popupcolor .swatchpop').each(function(idx, oElem){     
        var oBtn = jQuery(oElem);       
        var oColorBox = oBtn.parents('div.swatchBox1:first');       
        if (ddIsColorAvailable(getColorId(oElem), selectedSizeId() )) {         
            oBtn.attr('title', '').removeClass('unavailable'); 
            window.lillap.availableColors++;                    
            selectedId= "#colorboxpop-"+getColorId(oElem);     
        }
        else {       
            oBtn.attr('title', 'Unavailable For '+window.lillap.sizeElemName).addClass('unavailable');   
            jQuery('#curColor1').html('None Selected');     
            oColorBox.removeClass('selected');      
        }              
    });

    if(window.lillap.availableColors==1){
        
        var singleColorTestId="";
         var cur_sel=jQuery('.swatchBox.selected');
         var cur_selpop=jQuery('.swatchBox1.selected');        
         
        if(cur_sel.length==0 || cur_selpop.length==0)
        {
           console.log('if length = 0')
        }
        else{           
            
          var singleColorTest=document.getElementById('custcol_coloroption').value;
        
          singleColorTestId="#colorbox-"+singleColorTest;
          singleColorTestIdpop="#colorboxpop-"+singleColorTest;
        }
           
           if(singleColorTestId==""||singleColorTestId!=selectedId||singleColorTestIdpop==""||singleColorTestIdpop!=selectedId){
            
           window.lillap.singleColor=selectedId;
            
           if(window.lillap.userClick===0&&window.lillap.deselectSize===0){
          jQuery(selectedId).trigger( "click" );
         
          }

      }
          window.lillap.availableColors=0;
         


    }
    else{
        var cur_sel=jQuery('.swatchBox.selected');
        var cur_selpop=jQuery('.swatchBox1.selected');
        if(cur_sel.length==0 || cur_selpop.length==0){
            console.log('cur_sel ==0');

        }
         if(window.lillap.singleColor!="")
        {   
            deselectSwatch();
            window.lillap.singleColor="";
        }
        else{
        window.lillap.availableColors=0;
    }
       
    }
    
};

function ddIsColorAvailable (sColor, sizeSelected) {

    if (typeof(sizeSelected) === 'undefined') sizeSelected = selectedSizeId ();
    if(sizeSelected && typeof(window.lillap.aOutOfStock[sizeSelected+':'+sColor]) !== 'undefined') return false;
    
    return true;
};

function ddIsSizeAvailable (sSize, colorSelected) {
    if (typeof(colorSelected) === 'undefined') colorSelected = selectedColorId();
    if(colorSelected && typeof(window.lillap.aOutOfStock[sSize+':'+colorSelected]) !== 'undefined') return false;
   
    return true;
};

// *********  

var BASE_IMG_NAME_SPLIT = new RegExp('[0-9A-Za-z]+-([0-9A-Za-z]+)-([0-9A-Za-z]+)');
function itemAltImageSetup() {
    try
    {     
        window.lillap.mainImage="";
        window.lillap.zoomImage="";
        window.lillap.deselectSize=0;
        window.lillap.deselectSwatch=0;
         
         
        var aItemImageSplit, nInStock = 0;
        var aAvailableColors = Object.keys(lillap.oColorXref);
        lillap.sFirstBaseName = "";
        if (typeof(lillap.sItemImage) === "string" && lillap.sItemImage.length > 0) 
        {
            aItemImageSplit = BASE_IMG_NAME_SPLIT.exec(lillap.sItemImage);
            
            lillap.sFirstBaseName = aItemImageSplit[0];
            lillap.sPrimaryColor = aItemImageSplit[1];
            
        }
        if (lillap.aAltImageBase.length > 0 && lillap.aAltImageBase[0].length == 0) return; 
         // no alternates
        
        lillap.aAltImageBase.sort(altImageSortComparison);
        jQuery.each(lillap.aAltImageBase, function(idx, val) {
            var sSelected, sAbbr = /*val.substring(val.lastIndexOf('-')+1);*/
                                val.split('-')[1];            
            if (lillap.aColorIdInStock[lillap.oColorXref[sAbbr]] || lillap.sPrimaryColor == sAbbr)
            {
                var sSelected = val == lillap.sFirstBaseName ? "selected" : "";
                
                jQuery('#ddAltImageDiv').append('<div class="item-alt-cell"><a href="" onclick="return false;" class="item-alt-wrap '+sSelected+'"><span class="mini"><img src="/items/mini/'+val+'-MINI.jpg" ></span></a></div>');
                jQuery('#ddAltImageDiv .item-alt-cell:last-child').data('baseName', val).data('abbr', sAbbr );
               
                nInStock++;
            }
        }); 
        // only expose if there are alternates
        if (nInStock > 1) jQuery('#ddAltImageDiv').show();

        jQuery('.item-alt-cell').click(altItemClicked);
        jQuery('.item-alt-cell').bind('touchStart', altItemClicked);
      window.lillap.mainImage=jQuery('#mainImage').attr('src');
     
      window.lillap.zoomImage=jQuery('#ddZoomImageDiv img').attr('src');
      window.lillap.selectElem=jQuery('.item-alt-wrap:first').parent();
          


         var count=0;
          //logic for automatically dimming the unavailable sizes on page load
          for(var sizeIds=0;sizeIds<window.lillap.aAvailableSizeIds.length;sizeIds++)
          {
            window.lillap.unavailableCount=0;
           
            for(var colorIds=0;colorIds<window.lillap.aAvailableColorIds.length;colorIds++)
            {  
               
              var id=window.lillap.aAvailableSizeIds[sizeIds];
              if(ddIsColorAvailable(window.lillap.aAvailableColorIds[colorIds],window.lillap.aAvailableSizeIds[sizeIds]))
              {
                
                
              }
              else{
              
                window.lillap.unavailableCount++;
                
                if(window.lillap.unavailableCount==window.lillap.aAvailableColorIds.length)
                       
                    {  
                        var unavailableId="#size-"+id;
                        var unavailableIdpop="#sizepop-"+id;
                        jQuery(unavailableId).addClass("notavailable");
                        jQuery(unavailableId).attr("title","Unavailable");
                        jQuery(unavailableIdpop).addClass("notavailable");      
                        jQuery(unavailableIdpop).attr("title","Unavailable");
                        if(window.lillap.sizeDim.length==0)
                        {
                            window.lillap.sizeDim[0]=id;
                            
                        }
                        else
                        {
                            
                            count=count+1;
                            
                           window.lillap.sizeDim[count]=id;
                          
                        }

                      
                      
                    }
                    else{
                       
                    }

                    

                 }//else end

            }// Inner loop end


          }//OuterLoop end

         

          //Code for selecting single available color

          if(window.lillap.colorCount==1)
      
         { 
           
          var singleColorTestId="";
            var cur_sel=jQuery('.swatchbox.selected');
         if(cur_sel.length==0)
         {

         }
         else{
          var singleColorTest=document.getElementById('custcol_coloroption').value;
        
            singleColorTestId="#colorbox-"+singleColortest;
            singleColorTestpopId="#colorboxpop-"+singleColortest;
           }
            var selectedId= "#colorbox-"+window.lillap.colorCountId;
            var selectedpopId= "#colorboxpop-"+window.lillap.colorCountId;
            if(singleColorTestId==""||singleColorTestId!=selectedId||singleColorTestpopId==""||singleColorTestpopId!=selectedpopId){
            
          
          jQuery(selectedId).trigger( "click" );jQuery(selectedpopId).trigger( "click" );}
            window.lillap.colorCountId=0;
            window.lillap.colorCount=0;

         }

          //code for selecting single undimmed size
          if(window.lillap.sizeDim.length==0)
          {
            
          }
          else
          {
            
            if(window.lillap.sizeDim.length===window.lillap.aAvailableSizeIds.length-1)
            {
               
                
                var missingSize=  (window.lillap.aAvailableSizeIds).filter(function(n) {
                  return (window.lillap.sizeDim).indexOf(n) == -1
                    });
                
                var selectId="#size-"+missingSize;
                var cur_sel=jQuery('.sizeBox.selected');
                var selectIdpop="#sizepop-"+missingSize;
                
                if(cur_sel.length===0){
                jQuery(selectId).trigger("click");
                jQuery(selectIdpop).trigger("click");
               }
            }

          }
          
            
    }
    catch(e) { if (typeof(console) === "object") console.log("setupOptionsChooser exception:"+e); };
}

// Group colours, MAIN then ALTx 
function altImageSortComparison (a, b) {
    var aA, aB;
    if (a == lillap.sFirstBaseName) return -1; 
    if (b == lillap.sFirstBaseName) return 1;
    aA = BASE_IMG_NAME_SPLIT.exec(a);
    aB = BASE_IMG_NAME_SPLIT.exec(b); 
    if (aA[1] != aB[1]) { // different colors
        //TQ: give priority to sPrimaryColor
        if (aA[1] == lillap.sPrimaryColor) return -1;
        if (aB[1] == lillap.sPrimaryColor) return 1;
        //end TQ
        if (aA[1] < aB[1]) return -1;
        else return 1;
    } 
    else // same color
    {
        if ( aA[1] == "MAIN") { return -1; } // sort MAIN first
        //TQ: also check if aB is MAIN
        if ( aB[1] == "MAIN") { return 1; } // sort MAIN first
        //end TQ
        else {  // ALTx
            if (aA[1] < aB[1] ) { return -1; } 
            else return 1;
        }
    } 
}

function altItemClicked (oEvt) {
     
    
    window.lillap.colorSelect=1;
    window.lillap.mainImageLoad=0;
    deselectSwatch();
    
    ddSelectAltImage(this);
   
    
   // ddSelectSwatch(getColorIdFromAbbr(jQuery(this).data('abbr')), true); // indirect selection
}

function findAltImgElementFromColor ( sColorId ) {
    
    var oCurCell = jQuery('.item-alt-wrap.selected').parent();
    var sCurId = getColorIdFromAbbr(oCurCell.data('abbr'));
    //  if the currently selected image has the same color, do nothing
    if (sColorId == sCurId) return oCurCell;

    //  find the set of alt images matching the color 
    //      return the first MAIN image if one exists 
    //      else return the first ALTx image found
    var aColorElem = new Array();
    jQuery('.item-alt-cell').each(function(idx, elem){
        if (sColorId == lillap.oColorXref[jQuery(this).data('abbr')])
        {
            aColorElem.push(this);
        }
    });
    return aColorElem[0] ;

    
}

// expects .item-alt-cell
function ddSelectAltImage (oElem) {
    
   
    if (null == oElem) return;
    jQuery('.item-alt-wrap').removeClass('selected');   
    jQuery('.item-alt-wrap',oElem).addClass('selected');
    
    var sBaseName = jQuery(oElem).data('baseName');
    if(window.lillap.mainImageLoad===1)
    {   
        jQuery('#mainImage').attr('src',window.lillap.mainImage);
        jQuery('#ddZoomImageDiv img').attr('src',window.lillap.zoomImage);
        
        jQuery('.item-alt-wrap').removeClass('selected');   
        jQuery('.item-alt-wrap',window.lillap.selectElem).addClass('selected');
      
        
    }

    if(typeof(sBaseName) === "undefined"&& window.lillap.mainImageLoad===0)
    {   
        jQuery('#mainImage').attr('src',window.lillap.mainImage);
        jQuery('#ddZoomImageDiv img').attr('src',window.lillap.zoomImage);
        jQuery('.item-alt-wrap').removeClass('selected');
        jQuery('.item-alt-wrap',window.lillap.selectElem).addClass('selected');
        
       
    }
    if(typeof(sBaseName) != "undefined"&& window.lillap.mainImageLoad===0)
    {
        
     
    // set main image
    jQuery('#mainImage').attr('src', '/items/item/'+sBaseName+'-ITEM.jpg');
        // set enlarged image
        jQuery('#ddZoomImageDiv img').attr('src', '/items/image/'+sBaseName+'.jpg');
        if(window.lillap.colorSelect===1){
            var test=BASE_IMG_NAME_SPLIT.exec(sBaseName);
            var test1=test[2];
            jQuery.each(lillap.oColorXref,function(key,value){
                 
                if(test1===key)
                {                  
                    var selColor=document.getElementById('custcol_coloroption').value;
                    
                    var selectId="#colorbox-"+value;                  
                    
                    jQuery(selectId).trigger("click");
                }
            });
        }
    }
}
/*Start of Coding for send a hint popup*/
function ddSizepopSelected (parsedsizeid) {
    
        var oEvts = jQuery('.sizeBox');
        window.lillap.userClick=0;
        window.lillap.availableColors=0;
        var sizeidparse = jQuery('#sizepop-'+parsedsizeid);

       if(oEvts.hasOwnProperty('originalEvent'))
        {
          window.lillap.colorSelect=0;
        }
        else
        {
            
        }  
        if ( ! ddIsSizeAvailable(jQuery('#sizepop-'+parsedsizeid).attr('id').split('-')[1]) ) {
            
            deselectSwatch();
            ddSetSizeAffordances();       
        }
        var selects = jQuery('#sizepop-'+parsedsizeid).parents('div.sizeBox:first');
        var cur_selections = jQuery('.sizeBox.selected');

        var sizeSelElems = document.getElementById('custcol_size');
        if (selects && selects.length > 0 && cur_selections && cur_selections.length > 0 && selects[0] === cur_selections[0] ) {
            deselectSize();
             
            window.lillap.deselectSize=1;
            jQuery('#curSize').html(NONE_SELECTED);
            jQuery('#curSize1').html(NONE_SELECTED);
            setSelectValue(sizeSelElems, ''); 
            selected.removeClass('selected');
             // css('border-color','transparent');
            var x= window.lillap.aAvailableSizeIds.filter(function(n) {
                return window.lillap.sizeDim.indexOf(n) == -1;
            });
            var col_cur_sel=jQuery('.swatchBox1.selected');  //If a color is already selected donot refresh sizes
            
            for(var refreshs=0;refreshs<x.length;refreshs++)
            {
                var selectIds="#sizepop-"+x[refreshs];            
                jQuery(selectIds).attr("title","").removeClass("unavailable");
            }
            deselectSwatch();
            
            jQuery('#mainImage').attr('src',window.lillap.mainImage);
            jQuery('#ddZoomImageDiv img').attr('src',window.lillap.zoomImage);
            
            jQuery('.item-alt-wrap').removeClass('selected');   
            jQuery('.item-alt-wrap',window.lillap.selectElem).addClass('selected');
           

        } 
        else // new selection
        {   
            deselectSize();
            //console.log('size selected');
            window.lillap.deselectSize=0;
            var nSizes = parsedsizeid; 
            jQuery('#curSize').html(getSelectTextForValue(sizeSelElems,nSizes));
            var sizeselect = jQuery('#curSize1').html(getSelectTextForValue(sizeSelElems, nSizes));
            window.lillap.sizeElemName=getSelectTextForValue(sizeSelElems,nSizes);
            setSelectValue(sizeSelElems, nSizes); 
            selects.addClass('selected'); // css('border-color','black'); 
        }
        ddSetColorAffordances();
};
function buildColorHtml1 (nColorId) {
    //console.log('buildColorHtml1');
    var html = "<div class='swatchBox swatchBox1'><a href='javascript: void {};' class='swatchLink'><div class='swatchColor swatchpop' id='colorboxpop-"+nColorId+"'>&nbsp;</div></a></div>";
    window.lillap.colorCount++;
    window.lillap.colorRefresh[window.lillap.colorCount-1]=nColorId;

    if(window.lillap.colorCount==1)
    {
        window.lillap.colorCountId=nColorId;
    }     
        
    return html;
};
function buildSizeHtmlpop (nSizeId) {
    //var aSizeLbl = ['Error','XS','S','M','L','XL','OS','PS','ML'];

    var aSizeLbl = ['Error','XS','S','M','L','XL','OS','PS','ML','S/M','M/L','00','02','04','06','08','10','12','14'];
    
    var html = "<div class='sizeBox'><a href='javascript: void {};' class='sizeLink'><div class='sizeButton' id='sizepop-"+nSizeId+"'>"+
        aSizeLbl[nSizeId]+"</div></a></div>";
    return html;
};
function hint_fancy(){
    count++;
    if (jQuery('.hint_1').css('display')=='none' || count>1) {
       clickcount = 0;
        jQuery('.hint_1').css('display','block');
        jQuery('.hint_2').css('display','none');
        /* window.opener.$(".fancybox-wrap").load(".hint_1");*/
        jQuery('#input_firstname').val('');
        jQuery('#hint_from_email').val('');
        jQuery('#hint_email').val('');
        jQuery('#hint_subject').val('');
        jQuery('#curSize1').html('None Selected');
        jQuery('.swatchBox1.selected').removeClass('selected');
        jQuery('.sizeBox.selected').removeClass('selected');
        jQuery('#curColor1').html('None Selected'); 
        triggerAgain++; 
        jQuery('.form-errors').css('display','none');
        jQuery('.textarea').val(''); 
        jQuery('.hint_1').css('top','25px !important');
        /*jQuery('.hint_1').css({'position':'relative','top':'29px','height':'536px'}); 
        $('.hint_1').parents().css('overflow','hidden  !important'); */               
    }
     
    //console.log('count '+count);
    /*jQuery("#send_hints").css('display','block');*/
    jQuery('#send_hint').fancybox({
        'autoScale': true,
        'transitionIn': 'elastic',
        'transitionOut': 'elastic',
        'speedIn': 500,
        'speedOut': 300,
        'centerOnScrollolling' : 'auto',
        'autoDimensions': true,
        'centerOnScroll': true,
        'href' : '#send_hints',
        'type' :'inline',
        'width': 700,
        'height': 900,
        'helpers'     : { 
            'overlay' : {'closeClick': false}
        },
         beforeShow: function(){
            jQuery("body").css({'overflow-y':'hidden'});
        },
        afterClose: function(){
            jQuery("body").css({'overflow-y':'visible'});

        }
    }); 
    var fancy_style = jQuery( "#send_hints" ).parents( ".fancybox-inner" );    
    fancy_style.addClass('hint_fancybox');

    var sColorHTML1 = "";
        var nOutCount = 0;
        var nItemId = document.getElementById('buyid').value;
        var aAvailableSizeIds = getSelectValueArray(document.getElementById('custcol_size')).slice(1);
        var aAvailableColorIds = getSelectValueArray(document.getElementById('custcol_coloroption')).slice(1);
        var aOutOfStock = eval('Item'+nItemId+'_avail');
  var w = window; 
        w.lillap.aOutOfStock = aOutOfStock; 
        w.lillap.aColorIdInStock = new Array();
        for (var i=0; i < aAvailableColorIds.length; i++) {
            nOutCount = 0;
            for (var j=0; j < aAvailableSizeIds.length; j++) {
                if(aOutOfStock[aAvailableSizeIds[j]+':'+aAvailableColorIds[i]]) {
                    nOutCount++;
                }
            };
            if(nOutCount < aAvailableSizeIds.length) {
                w.lillap.aColorIdInStock[aAvailableColorIds[i]] = true;
                sColorHTML1 += buildColorHtml1( aAvailableColorIds[i] ); // color in stock
            } 
        };
        if (count == 1) {
        jQuery('.popupcolor').append(sColorHTML1).mouseleave( ddHideSwatch );
        }
    //console.log('aAvailableColorIds '+aAvailableColorIds);
    var parent_color_map = JSON.parse(jQuery('#parent_color_map').val());
    //console.log('parent_color_map '+parent_color_map);
    var url = '/items/swatch/';
    
    var colorbox, colorbox_class, rgb, sName;    
    for (var i = 0; i < aAvailableColorIds.length; i++) {
         
        w.lillap.colorData = parent_color_map;
        
        colorbox = jQuery('#colorboxpop-'+aAvailableColorIds[i]);
        
        //Check that the color exists in the parent color map, otherwise, write the name
        if (parent_color_map[aAvailableColorIds[i]]) {
            //console.log('parent_color_map enetered ');
            colorbox_class = parent_color_map[aAvailableColorIds[i]].abbreviation ? parent_color_map[aAvailableColorIds[i]].abbreviation : 'NNN';
            parent_color_map[aAvailableColorIds[i]].id = aAvailableColorIds[i];
            if (parent_color_map[aAvailableColorIds[i]].swatch.length > 0){
                colorbox.addClass('imgSrc');
                parent_color_map[aAvailableColorIds[i]].url = url + parent_color_map[aAvailableColorIds[i]].swatch ;
                colorbox.html('<img src="'+url+parent_color_map[aAvailableColorIds[i]].swatch+'" data-pin-nopin="true">' );
                //console.log(colorbox.html());
            }
            
            rgb = parent_color_map[aAvailableColorIds[i]].rgb ? parent_color_map[aAvailableColorIds[i]].rgb : 'FFF';
            sName = parent_color_map[aAvailableColorIds[i]].name ? parent_color_map[aAvailableColorIds[i]].name : 'Color Name';
            colorbox.data(parent_color_map[aAvailableColorIds[i]]);
        } 
        else{
            //console.log('else');
            //Do something
            colorbox_class ='';
            rgb = '';
            sName = 'Color Name';
            colorbox.data({"class": aAvailableColorIds[i], "name":"Color Name","rgb":"FFF","swatch":"","abbreviation":"NNN","source":"w"});
        }      
        //console.log(colorbox.html());
        //colorbox.addClass(colorbox_class);
        colorbox.css('background-color','#'+rgb);
        colorbox.attr('title', sName); 
        colorbox.unbind('click').click(ddSwatchClickedpop);         
        // was on parent         
        window.lillap.sizeDim.length=0;
    };  
    
}
function ddSwatchClickedpop ( oEvt ) {
    clickcount++
    window.lillap.userClick=0;  
    var oTgt = jQuery(oEvt.target);
    var nClrId = getColorId(oEvt.target);
    window.lillap.availableSizes=0;
    var sBaseName = '';
    var jElemes = jQuery('#colorboxpop-'+nClrId);
    var selector = jElemes.parents('div.swatchBox1:first');
    jQuery('.swatchbox1').addClass('selectedswatch');
    if(oEvt.hasOwnProperty('originalEvent'))
    {    
        window.lillap.userClick++;
        window.lillap.colorSelect=0;
    } 
    else{
    }
    if (oTgt.hasClass('swatchpop')) {
        ddSelectSwatchpop(nClrId);
    } 
    else
    {
        nClrId = getColorId(oTgt.parent());
        ddSelectSwatchpop(nClrId); 
        // on img, get parent swatchColor div        
    }   
     
    if(window.lillap.colorSelect===0){
        //console.log('clickcount  is '+clickcount);
        if (clickcount>1) {
            var sameColor = '';
            var sameId = '';
            var replaceColor = '';
            var replaceId = '';
            var sameclrbg = '';
            var replacebg = '';
            var sameColorwithComma = '';
            var sameColorwithbeforeComma = '';
            //console.log('nClrId '+nClrId);
            var curcolor = jQuery('#colorname').val();
            var colorid = jQuery('#bgcolorid').val();
            var colorName = jQuery('#colorboxpop-'+nClrId).data('name'); 
            /*var id = colorid.split(',');*/
            var bgclr = jQuery('#bgcolor').val();           
            var name = curcolor.split(',');
            var bgclrsplit = bgclr.split('-');
            var clreach = jQuery('#colorboxpop-'+nClrId).css("background-color");
            for (var j=0;j<name.length;j++) {
                
                if (name[j] == colorName /*&& id[j] == nClrId*/ ){ 
                    sameColor=name[j];
                    sameColorwithComma = name[j+1];
                    sameColorwithbeforeComma = name[j-1];
                    // sameId = id[j];
                    sameclrbg = bgclrsplit[j];
                    if (curcolor == '' && colorid == '') {
                        replacebg = bgclr.replace(''+sameclrbg,'');
                        jQuery('#bgcolor').val(replacebg);                                              
                        replaceColor = curcolor.replace(''+sameColor,'');                       
                    }
                    else{
                        if (firstSelect == sameColor) {
                            if(bgclr.search('-')==-1){
                               replacebg = bgclr.replace(sameclrbg,'');
                               jQuery('#bgcolor').val(replacebg);
                                replaceColor = curcolor.replace(sameColor,''); 
                            }
                            else{
                                
                                if (sameColorwithComma == undefined) {
                                    replaceColor = curcolor.replace(sameColor,'');
                                    replacebg = bgclr.replace(sameclrbg,'');
                                    jQuery('#bgcolor').val(replacebg); 
                                }
                                else{
                                    replaceColor = curcolor.replace(sameColor+',','');
                                    replacebg = bgclr.replace(sameclrbg+'-','');
                                    jQuery('#bgcolor').val(replacebg);
                                }                         
                            }                            
                        }                         
                        else{
                            bgclr = jQuery('#bgcolor').val(); 
                            if(bgclr.search('-') == -1){
                               replacebg = bgclr.replace(sameclrbg,'');
                                jQuery('#bgcolor').val(replacebg);
                                replaceColor = curcolor.replace(sameColor,'');
                            }else{
                                
                                if (sameColorwithbeforeComma == undefined) {
                                    replaceColor = curcolor.replace(sameColor+',','');
                                    replacebg = bgclr.replace(sameclrbg+'-','');
                                    jQuery('#bgcolor').val(replacebg);
                                }
                                else{
                                    replacebg = bgclr.replace('-'+sameclrbg,'');
                                    jQuery('#bgcolor').val(replacebg);
                                    replaceColor = curcolor.replace(','+sameColor,'');
                                }                          
                            }                           
                        }
                    }
                    var curcolor = jQuery('#colorname').val(replaceColor);
                    if (jQuery('#curColor1').html()=='') {
                        jQuery('#curColor1').html(NONE_SELECTED);
                    }else{
                        jQuery('#curColor1').html(replaceColor);
                    }                    
                    jQuery('#bgcolor').val(replacebg);                    
                }
            } 
            if (colorName != '' && sameColor == '' ) {

                var jE = jQuery('#colorboxpop-'+nClrId);
                var sel = jE.parents('div.swatchBox1:first');
                sel.addClass('selected');
                if (jQuery('#curColor1').html()=='None Selected') {
                    jQuery('#curColor1').html(colorName);
                    jQuery('#colorname').val(colorName);
                    if (jQuery('#colorboxpop-'+nClrId).hasClass('imgSrc')) {

                        var srcData = jQuery('#colorboxpop-'+nClrId+' img').attr('src');
                        jQuery('#bgcolor').val(clreach+'|'+srcData); 
                    }
                    else{
                        jQuery('#bgcolor').val(clreach); 
                    }
                }else{
                    
                    jQuery('#curColor1').html(''+curcolor+','+colorName);
                    jQuery('#colorname').val(''+curcolor+','+colorName);
                    /*jQuery('#bgcolorid').val(''+colorid+','+nClrId);*/
                    if (jQuery('#colorboxpop-'+nClrId).hasClass('imgSrc')) {

                        var srcData = jQuery('#colorboxpop-'+nClrId+' img').attr('src');
                        jQuery('#bgcolor').val(''+bgclr+'-'+clreach+'|'+srcData);
                    }
                    else{
                        jQuery('#bgcolor').val(''+bgclr+'-'+clreach);
                    }                     
                }   
            }
            else if (sameColor!='' ) {

                if (jQuery('#curColor1').html()=='') {
                    jQuery('#curColor1').html(NONE_SELECTED);
                }
                var jE = jQuery('#colorboxpop-'+nClrId);
                var sel = jE.parents('div.swatchBox1:first');
                sel.removeClass('selected');                
            }                        
        }
        else{
            jQuery('#bgcolorid').val(nClrId);
            var bgcolor12 = jQuery('#colorboxpop-'+nClrId).css("background-color");
            if (jQuery('#colorboxpop-'+nClrId).hasClass('imgSrc')) {

                var srcData = jQuery('#colorboxpop-'+nClrId+' img').attr('src');
                jQuery('#bgcolor').val(bgcolor12+'|'+srcData);
            }
            else{
                jQuery('#bgcolor').val(bgcolor12);
            }            
        }        
        /*if (sameColor!='' && clickcount == 2 ) {
            //console.log('clickcount sameColor '+clickcount);
            jQuery('#curColor1').html(NONE_SELECTED);
            selector.removeClass('selected');
            jQuery('#colorname').val('');
            /*jQuery('#bgcolorid').val('');*/
            /*jQuery('#bgcolor').val('');*/
        /*}*/     
        var altid = findAltImgElementFromColor(nClrId);
        if (null == altid) return;        
        jQuery('.swatchBox1.selected').attr('title'); 
        sBaseName = jQuery(altid).data('baseName'); 
        /*console.log('sBaseName '+sBaseName);*/
        jQuery('#sBaseName').val(sBaseName); 

        var bgcolor = jQuery('#colorboxpop-'+nClrId).css("background-color");
        /*console.log('bgcolor '+bgcolor);*/                       
    }    
}
function ddSelectSwatchpop ( sColorId, bIndirect ) {   
     
    var jElem = jQuery('#colorboxpop-'+sColorId),
        sName = '',
        sAbbr = '';
    sName = jElem.data('name');     
    sAbbr = jElem.data('abbr'); 
    
    window.lillap.colorElemName=sName;
    if ( ! ddIsColorAvailable(sColorId) ) {
        deselectSize();
        ddSetColorAffordances ();      
    }
    var colorSelElem = document.getElementById('custcol_coloroption');
   
    var selected = jElem.parents('div.swatchBox1:first');    
    // Which was clicked
    var cur_selection = jQuery('.swatchBox1.selected');    
    // What was selected
    var cur_color = getColorId(jQuery('.swatchColor', cur_selection));
     
    if (bIndirect && (sColorId == cur_color))
    {
        // leave currently selected color selected
    }
    else {
        if (clickcount < 2 && selected && selected.length > 0 && cur_selection && cur_selection.length > 0 && selected[0] === cur_selection[0] ) {
            deselectSwatch();
            //console.log('clickcount < 2');
            jQuery('#curColor1').html(NONE_SELECTED);
            selected.removeClass('selected'); //.css('border-color','transparent');
            setSelectValue(colorSelElem, '');         
            window.lillap.mainImageLoad=1;
            window.lillap.deselectSwatch=1;

            for(var refresh=0;refresh<window.lillap.colorRefresh.length;refresh++)
            {               
                var selectId="#colorboxpop-"+window.lillap.colorRefresh[refresh];
                jQuery(selectId).removeClass("unavailable");
                jQuery(selectId).attr('title','');
            }
            deselectSize();
        } 
        else // new selection
        {   
            window.lillap.deselectSwatch=0;
            deselectSwatch();
            window.lillap.mainImageLoad=0;
            if (clickcount == 1) {
                jQuery('#colorname').val(sName);
                jQuery('#curColor1').html(sName);
                selected.addClass('selected'); // .css('border-color','black');
                setSelectValue(colorSelElem, sColorId);
                firstSelect = sName;                
            } 
        }
    }
    ddSetSizeAffordancespop ();
};
function ddSetSizeAffordancespop () {
    var selectedId;
    window.lillap.availableSizes=0;  

    jQuery('.popupsize .sizeButton').each(function(idx, oElem){
        var oBtn = jQuery(oElem);
        var unavailableConstant=0;
        var oSizeBox = oBtn.parents('div.sizeBox:first');
        
        if (ddIsSizeAvailable(getSizeId(oElem), selectedColorId() )) {    
            
          
           if(oBtn.hasClass("notavailable"))
           {
            oBtn.attr("title","Unavailable");
           }

            window.lillap.availableSizes++;
           oBtn.attr("title","").removeClass('unavailable');  
           selectedId= "#sizepop-"+getSizeId(oElem);
        } 
        else {
           
            oBtn.attr('title', 'Unavailable');
           
            if(oBtn.hasClass("notavailable"))
            {

            }
            else{
            oBtn.attr("title","Unavailable For "+window.lillap.colorElemName).addClass('unavailable');
            
            }
        }
    });
    
    if(window.lillap.availableSizes==1){
       
        var singleSizeTestId=""; 
        
        var cur_sel=jQuery('.sizeBox.selected');      
        if(cur_sel.length==0)
        {
           
        }
        else{
            
            var singleSizeTest=document.getElementById('custcol_size').value;
        
            singleSizeTestId="#sizepop-"+singleSizeTest;
        }
           
        if(singleSizeTestId==""||singleSizeTestId!=selectedId){
           window.lillap.singleSize=selectedId;
           
           if(window.lillap.deselectSwatch===0){
          jQuery(selectedId).trigger( "click" );}
        }     
          window.lillap.availableSizes=0;
        }
    else{
        if(window.lillap.singleSize!="")
        {   
            deselectSize();
             window.lillap.singleSize="";
        }
        else{
            window.lillap.availableSizes=0;
        }
    }
};
/*End of Coding for send a hint popup*/
function salePriceCustomizations () {
    try 
    {
        var sSalePrice = parseFloat(jQuery.trim(jQuery('.dd-saleprice').html()));
        var sMarkdownPrice = parseFloat(jQuery.trim(jQuery('.dd-markdownprice').html()));
        
        if (sMarkdownPrice > 0.00 && sSalePrice > 0.00) {

            if (sSalePrice <= sMarkdownPrice) {
               
                jQuery('.dd-price').css('font-weight','normal');
                jQuery('#dd-listprice').css('text-decoration', 'line-through'); // .prepend('Was ');
                jQuery('.dd-saleprice').prepend('$'); // .css('font-size', '12pt');
                jQuery('.dd-markdownprice').hide();
            }
             else {
                
                jQuery('.dd-price').css('font-weight','normal');
                jQuery('#dd-listprice').css('text-decoration', 'line-through'); // .prepend('Was ');
                jQuery('.dd-markdownprice').prepend('$'); // .css('font-size', '12pt');
                jQuery('.dd-saleprice').hide();
            }
        }
         else
        {
            //console.log('NaN');
            if (sMarkdownPrice>0.00) {
                //console.log(sMarkdownPrice+'>'+0.00);
                jQuery('.dd-price').css('font-weight','normal');
                jQuery('#dd-listprice').css('text-decoration', 'line-through'); // .prepend('Was ');
                jQuery('.dd-markdownprice').prepend('$'); // .css('font-size', '12pt');
                jQuery('.dd-saleprice').hide();
            }
            else if (sSalePrice>0.00) {
                //console.log(sSalePrice+'>'+0.00);
                //console.log(sSalePrice>0);
                jQuery('.dd-price').css('font-weight','normal');
                jQuery('#dd-listprice').css('text-decoration', 'line-through'); // .prepend('Was ');
                jQuery('.dd-saleprice').prepend('$'); // .css('font-size', '12pt');
                jQuery('.dd-markdownprice').hide();
            }
        }            
        if ("Yes" == jQuery('#item_final_sale').val())
        {
            jQuery('#dd-messages').append(jQuery('#txt-finalsale')); 
        }
        if ("Yes" == jQuery('#sale_credit_only').val())
        {
            jQuery('#dd-messages').append(jQuery('#txt-creditonly')); 
        }
    }
    catch(e)
    {
        
    }   
} // salePriceCustomizations